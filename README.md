# Drawing

## What It Is
A simple console version of a drawing program. The functionality of the program is quite limited but this might change in the future.


## How It Works
In a nutshell, the program works as follows:
* Create a new canvas
* Start drawing on the canvas by issuing various commands
* Quit

| Command         | Description                                                                                                                                                                       |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `C w h`         | Create a new canvas of width w and height h.                                                                                                                               |
| `L x1 y1 x2 y2` | Create a new line from (x1,y1) to (x2,y2). Currently only horizontal or vertical lines are supported. Horizontal and vertical lines will be drawn using the 'x' character. |
| `R x1 y1 x2 y2` | Create a new rectangle, whose upper left corner is (x1,y1) and lower right corner is (x2,y2). Horizontal and vertical lines will be drawn using the 'x' character.         |
| `B x y c`       | Fill the entire area connected to (x,y) with "colour" c. The behaviour of this is the same as that of the "bucket fill" tool in paint programs.                            |
| `Q`             | Quit the program.                                                                                                                                                          |


## I/O Example
Below is a sample run of the program. User input is prefixed with enter command:

```
enter command: C 20 4
----------------------
|                    |
|                    |
|                    |
|                    |
----------------------

enter command: L 1 2 6 2
----------------------
|                    |
|xxxxxx              |
|                    |
|                    |
----------------------

enter command: L 6 3 6 4
----------------------
|                    |
|xxxxxx              |
|     x              |
|     x              |
----------------------

enter command: R 14 1 18 3
----------------------
|             xxxxx  |
|xxxxxx       x   x  |
|     x       xxxxx  |
|     x              |
----------------------

enter command: B 10 3 o
----------------------
|oooooooooooooxxxxxoo|
|xxxxxxooooooox   xoo|
|     xoooooooxxxxxoo|
|     xoooooooooooooo|
----------------------

enter command: Q
```


## Usage and Development

### To run the program, simply input `$ npm start`.

Below are useful commands for development.

```
Run test
$ npm test

Run lint
$ npm run lint

Run lint with auto fix
$ npm run lintfix
```

### Testing

There is only a simple unit test for `Canvas`, and almost no E2E test written, which can be improved in the future.


## Todos

* Support drawing lines with different angles other than vertical and horizontal.

* Add more shapes

* Filling algorithm is a simple recursive one, which is ok for a console-only mini program, but will be a problem in larger scale.