const assert = require("assert").strict;
const sinon  = require("sinon");
const child = require("child_process");

const Canvas = require("../canvas");

describe("index", () => {
  describe("I/O", () => {
    it("should ask for input on start", () => {
      child.execSync("node index.js", (err, stdout) => {
        // "spy" on `console.log()`
        const spy = sinon.spy(stdout, "write");
        
        const expected_output = "enter command:";

        // restore the original function
        spy.restore();

        assert.equal(spy.args[0], expected_output);
      });
    });
  });

  // TODO: add more tests for command line interactions
});


describe("Canvas", () => {
  let canvas;

  describe("constructor()", () => {
    it("should create without error", () => {
      canvas = new Canvas(5, 3);
      assert.ok(canvas);
      assert.equal(canvas.content.length, 3);
      assert.equal(canvas.content[0].length, 5);
    });

    it("should throw error when input dimension is missing", () => {
      assert.throws(() => {
        canvas = new Canvas();
      },
      {
        name: "Error"
      });
    });

    it("should throw error when input dimension is not integer", () => {
      assert.throws(() => {
        canvas = new Canvas("a", "b");
      },
      {
        name: "Error"
      });
    });

    it("should throw error when input dimension is not positive", () => {
      assert.throws(() => {
        canvas = new Canvas(-2, -1);
      },
      {
        name: "Error"
      });
    });
  });

  describe("print()", () => {
    it("should print through console", () => {
      // "spy" on `console.log()`
      const spy = sinon.spy(console, "log");
      canvas.print();
      
      const expected_output = [
        [ "-------" ],
        [ "|     |" ],
        [ "|     |" ],
        [ "|     |" ],
        [ "-------" ]
      ];

      // restore the original function
      spy.restore();

      assert.deepEqual(spy.args, expected_output);
    });
  });

  describe("drawLine()", () => {
    it("should draw a line", () => {
      canvas.drawLine(1, 2, 3, 2);
      assert.equal(canvas.content[1][0], "x");
      assert.equal(canvas.content[1][1], "x");
      assert.equal(canvas.content[1][2], "x");
      assert.equal(canvas.content[1][3], " ");
    });

    it("should refuse to draw a oblique line", () => {
      assert.throws(() => {
        canvas.drawLine(1, 2, 3, 4);
      },
      {
        name: "Error"
      });
    });
  });

  describe("constructor()", () => {
    it("should create new canvas without error", () => {
      canvas = new Canvas(5, 5);
      assert.ok(canvas);
      assert.equal(canvas.content.length, 5);
      assert.equal(canvas.content[0].length, 5);
    });
  });

  describe("drawRectangle()", () => {
    it("should draw a rectangle", () => {
      canvas.drawRectangle(1, 2, 3, 4);
      assert.deepEqual(canvas.content[1], ["x", "x", "x", " ", " "]);
      assert.deepEqual(canvas.content[2], ["x", " ", "x", " ", " "]);
      assert.deepEqual(canvas.content[3], ["x", "x", "x", " ", " "]);
    });

    it("should refuse to draw a rectangle when (x1, y1) is not upper left corner", () => {
      assert.throws(() => {
        canvas.drawRectangle(3, 4, 1, 2);
      },
      {
        name: "Error"
      });
    });
  });

  describe("fill()", () => {
    it("should fill empty space correctly", () => {
      canvas.fill(4, 1, "f");
      assert.deepEqual(canvas.content[0], ["f", "f", "f", "f", "f"]);
      assert.deepEqual(canvas.content[1], ["x", "x", "x", "f", "f"]);
      assert.deepEqual(canvas.content[2], ["x", " ", "x", "f", "f"]);
      assert.deepEqual(canvas.content[3], ["x", "x", "x", "f", "f"]);
      assert.deepEqual(canvas.content[4], ["f", "f", "f", "f", "f"]);
    });

    it("should fill colored space correctly", () => {
      canvas.fill(3, 2, "g");
      assert.deepEqual(canvas.content[0], ["f", "f", "f", "f", "f"]);
      assert.deepEqual(canvas.content[1], ["g", "g", "g", "f", "f"]);
      assert.deepEqual(canvas.content[2], ["g", " ", "g", "f", "f"]);
      assert.deepEqual(canvas.content[3], ["g", "g", "g", "f", "f"]);
      assert.deepEqual(canvas.content[4], ["f", "f", "f", "f", "f"]);
    });

    it("should refuse to fill color as \"x\"", () => {
      assert.throws(() => {
        canvas.fill(3, 2, "x");
      },
      {
        name: "Error"
      });
    });
  });
});