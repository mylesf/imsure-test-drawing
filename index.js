"use strict";

const readline = require("readline");
const Canvas = require("./canvas");
let canvas = null;

// initial setup
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.setPrompt("enter command: ");

clear();
rl.prompt(true);

// input listener
rl.on("line", (input) => {
  processNewInput(input);
});

function clear() {
  const stdout = process.stdout;
  if (!stdout.isTTY) return;
  stdout.write("\x1B[2J\x1B[0f");
}

function processNewInput(input) {
  input = input.trim();
  clear();
  processCommand(input, (success, errorKey, errorMsg) => {
    if (success) {
      console.log(`Last input: ${input}\n`);
      canvas.print();
    } else {
      showErrors(errorKey, errorMsg);
      if (canvas) {
        console.log("\n");
        canvas.print();
      }
    }
    rl.prompt(true);
  });
}

function showErrors(errorKey, errorMsg="") {
  switch(errorKey) {
  case "illegal_C":
    console.error(`Illegal input. ${errorMsg}`);
    printUsage("C", true);
    break;

  case "illegal_L":
    console.error(`Illegal input. ${errorMsg}`);
    printUsage("L", true);
    break;

  case "illegal_R":
    console.error(`Illegal input. ${errorMsg}`);
    printUsage("R", true);
    break;

  case "illegal_B":
    console.error(`Illegal input. ${errorMsg}`);
    printUsage("B", true);
    break;

  case "illegal":
    console.error(`Illegal input. ${errorMsg}`);
    printUsage("", true);
    break;

  case "canvas_need_init":
    console.error("Canvas not initiated. Enter \"C {w} {h}\" first.\n");
    break;

  default:
    console.error("Unknown error. Please try again.");
  }
}

function printUsage(command_key, prepend_intro) {
  if (prepend_intro) {
    console.log("\nUsage:\n");
  }

  switch(command_key) {
  case "C":
    console.log("C {w} {h}\n  Create a new canvas of width w and height h.");
    break;

  case "L":
    console.log("L {x1} {y1} {x2} {y2}\n  Create a new line from (x1,y1) to (x2,y2). Currently only horizontal or vertical lines are supported. Horizontal and vertical lines will be drawn using the 'x' character.");
    break;

  case "R":
    console.log("R {x1} {y1} {x2} {y2}\n  Create a new rectangle, whose upper left corner is (x1,y1) and lower right corner is (x2,y2). Horizontal and vertical lines will be drawn using the 'x' character.");
    break;

  case "B":
    console.log("B {x} {y} {c}\n  Fill the entire area connected to (x,y) with \"colour\" c. The behaviour of this is the same as that of the \"bucket fill\" tool in paint programs.");
    break;

  case "Q":
    console.log("Q\n  Quit the program.");
    break;

  default:
    printUsage("C");
    printUsage("L");
    printUsage("R");
    printUsage("B");
    printUsage("Q");
  }

  // extra line break
  console.log();
}

function wrongNumOfArgs(callback, args, expect_num_of_args) {
  // check number of args
  if (args.length !== expect_num_of_args) {
    return callback(false, `illegal_${args[0]}`, `Expecting ${expect_num_of_args} arguments, received ${args.length}.`);
  }
  return false;
}

function processCommand(command, callback) {
  // parse command
  const args = command.split(" ");

  // do command
  switch(args[0]) {
  case "C":
    if (wrongNumOfArgs(callback, args, 3)) {
      return;
    }

    try {
      canvas = new Canvas(args[1], args[2]);
    } catch (e) {
      return callback(false, "illegal_C", e.message);
    }
    callback(true);
    break;

  case "L":
    if (wrongNumOfArgs(callback, args, 5)) {
      return;
    }

    if (canvas) {
      try {
        canvas.drawLine(args[1], args[2], args[3], args[4]);
      } catch (e) {
        return callback(false, "illegal_L", e.message);
      }
    } else {
      return callback(false, "canvas_need_init");
    }
    callback(true);
    break;

  case "R":
    if (wrongNumOfArgs(callback, args, 5)) {
      return;
    }

    if (canvas) {
      try {
        canvas.drawRectangle(args[1], args[2], args[3], args[4]);
      } catch (e) {
        return callback(false, "illegal_R", e.message);
      }
    } else {
      return callback(false, "canvas_need_init");
    }
    callback(true);
    break;

  case "B":
    if (wrongNumOfArgs(callback, args, 4)) {
      return;
    }

    if (canvas) {
      try {
        canvas.fill(args[1], args[2], args[3]);
      } catch (e) {
        return callback(false, "illegal_B", e.message);
      }
    } else {
      return callback(false, "canvas_need_init");
    }
    callback(true);
    break;

  case "Q":
    console.log("Exiting\n");
    process.exit();
    break;

  default:
    return callback(false, "illegal");
  }
}