module.exports = class Canvas {
  constructor(width, height){
    // convert numeric input args to integer
    width = parseInt(width);
    height = parseInt(height);

    if (!(width > 0 && height > 0)) {
      throw Error("Height and width must be positive integers.");
    }

    this.width = width;
    this.height = height;
  
    /***
    * Init content as an array of array.
    *
    * ATTENTION: index starts from 0 here,
    * while our input will be from 1.
    ***/
    this.content = [...Array(this.height)].map(() =>
      [...Array(this.width)].map(() => " "));
  }

  drawLine(x1, y1, x2, y2) {
    const p1 = this.validateAndConvertCoordinate(x1, y1);
    const p2 = this.validateAndConvertCoordinate(x2, y2);
    x1 = p1[0];
    y1 = p1[1];
    x2 = p2[0];
    y2 = p2[1];

    // only allow vertical or horizontal lines
    if (x1 !== x2 && y1 !== y2) {
      throw Error("Currently only horizontal or vertical lines are supported.");
    }

    // All validated.
    if (x1 === x2) {
      const y_from = Math.min(y1, y2);
      const y_to = Math.max(y1, y2);

      // draw vertical line
      for (let y=y_from; y<=y_to; y++) {
        this.content[y][x1] = "x";
      }
    } else {
      const x_from = Math.min(x1, x2);
      const x_to = Math.max(x1, x2);

      // draw horizontal line
      for (let x=x_from; x<=x_to; x++) {
        this.content[y1][x] = "x";
      }
    }
  }


  drawRectangle(x1, y1, x2, y2) {
    const p1 = this.validateAndConvertCoordinate(x1, y1);
    const p2 = this.validateAndConvertCoordinate(x2, y2);
    x1 = p1[0];
    y1 = p1[1];
    x2 = p2[0];
    y2 = p2[1];

    // only allow p1 to be upper left corner
    if (x1 >= x2 || y1 >= y2) {
      throw Error("Point (x1, y1) must be the upper left corner.");
    }

    // All validated.
    for (let x=x1; x<=x2; x++) {
      // draw top border
      this.content[y1][x] = "x";
      // draw bottom border
      this.content[y2][x] = "x";
    }

    for (let y=y1+1; y<y2; y++) {
      // draw left border
      this.content[y][x1] = "x";
      // draw right border
      this.content[y][x2] = "x";
    }
  }


  fill(x, y, color) {
    const p = this.validateAndConvertCoordinate(x, y);
    x = p[0];
    y = p[1];

    if (!color) {
      throw Error("You must input fill color.");
    }

    color = color.trim();

    if (color.length !== 1) {
      throw Error("Fill color must be a single charactor.");
    }

    if (color === "x") {
      throw Error("Letter \"x\" is reserved and cannot be used as fill color.");
    }

    this.fillWorker(x, y, this.content[y][x], color);
  }


  fillWorker(x, y, from_color, to_color) {
    /* ATTENTION: here x / y are internal values thus start from 0 */

    // out of range, skip
    if (x < 0 || x >= this.width || y < 0 || y >= this.height) return;

    // same colors, nothing to change
    if (from_color === to_color) return;

    // this node is the border, we should not touch
    if (this.content[y][x] !== from_color) return;

    // paint current node
    this.content[y][x] = to_color;

    /* continue exploring in 4 directions */
    // go up
    this.fillWorker(x, y-1, from_color, to_color);
    
    // go down
    this.fillWorker(x, y+1, from_color, to_color);
    
    // go left
    this.fillWorker(x-1, y, from_color, to_color);

    // go right
    this.fillWorker(x+1, y, from_color, to_color);

  }


  // validate as coordinate values,
  // and minus one to match our "content" matrix index
  validateAndConvertCoordinate(x, y) {
    x = parseInt(x);
    y = parseInt(y);

    // validate positive int
    if (!(x > 0 && y > 0)) {
      throw Error("Coordinates must be positive integers.");
    }

    // validate range within canvas
    if (x > this.width || y > this.height) {
      throw Error(`Coordinates must be within the canvas size (${this.width} x ${this.height}) `);
    }

    return [--x, --y];
  }

  print() {
    // top line
    console.log(Array(this.width+3).join("-"));

    for (let y=0; y<this.height; y++) {
      let line = "";

      // left border
      line += "|";

      for (let x=0; x<this.width; x++) {
        line += this.content[y][x];
      }

      // right border
      line += "|";

      // print line
      console.log(line);
    }

    // bottom line
    console.log(Array(this.width+3).join("-"));
  }
};